package ru.t1.gorodtsova.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUnbindFromProjectRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    @Nullable
    private String taskId;

    public TaskUnbindFromProjectRequest(@Nullable String token, @Nullable String projectId, @Nullable String taskId) {
        super(token);
        this.projectId = projectId;
        this.taskId = taskId;
    }

}
