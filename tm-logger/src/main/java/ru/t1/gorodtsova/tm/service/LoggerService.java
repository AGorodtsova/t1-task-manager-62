package ru.t1.gorodtsova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.gorodtsova.tm.api.IPropertyService;

import java.util.LinkedHashMap;
import java.util.Map;

@Service
public final class LoggerService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @SneakyThrows
    public void log(@NotNull final String text) {
        @NotNull final String mongoHost = propertyService.getMongoHost();
        @NotNull final Integer mongoPort = propertyService.getMongoPort();
        @NotNull final MongoClient mongoClient = new MongoClient(mongoHost, mongoPort);
        @NotNull final MongoDatabase mongoDatabase = mongoClient.getDatabase("tm_logger");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Map<String, Object> event = objectMapper.readValue(text, LinkedHashMap.class);
        @NotNull final String collectionName = event.get("table").toString();
        if (mongoDatabase.getCollection(collectionName) == null) mongoDatabase.createCollection(collectionName);
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
        collection.insertOne(new Document(event));
    }

}
