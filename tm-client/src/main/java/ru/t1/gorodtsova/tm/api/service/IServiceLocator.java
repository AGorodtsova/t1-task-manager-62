package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    IUserEndpoint getUserEndpoint();

    ITokenService getTokenService();

}
