package ru.t1.gorodtsova.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.dto.request.domain.DataXmlLoadFasterXmlRequest;
import ru.t1.gorodtsova.tm.event.ConsoleEvent;

@Component
public final class DataXmlLoadFasterXmlListener extends AbstractDataListener {

    @NotNull
    private final String DESCRIPTION = "Load data from xml file";

    @NotNull
    private final String NAME = "data-load-xml";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@dataXmlLoadFasterXmlListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataXmlLoadFasterXmlRequest request = new DataXmlLoadFasterXmlRequest(getToken());
        domainEndpoint.loadDataXmlFasterXml(request);
    }

}
