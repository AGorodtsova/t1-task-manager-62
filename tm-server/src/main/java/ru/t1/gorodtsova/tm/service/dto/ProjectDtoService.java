package ru.t1.gorodtsova.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.gorodtsova.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.gorodtsova.tm.api.service.dto.IProjectDtoService;
import ru.t1.gorodtsova.tm.comparator.CreatedComparator;
import ru.t1.gorodtsova.tm.comparator.StatusComparator;
import ru.t1.gorodtsova.tm.dto.model.ProjectDTO;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.exception.entity.ModelNotFoundException;
import ru.t1.gorodtsova.tm.exception.field.*;

import java.util.Comparator;
import java.util.List;

@Service
public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDTO>
        implements IProjectDtoService {

    @NotNull
    @Autowired
    private IProjectDtoRepository repository;

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(name);
        return add(userId, project);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setName(name);
        project.setDescription(description);
        return add(userId, project);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final Comparator<ProjectDTO> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        return repository.findAllByUserIdWithSort(userId, getSortedColumn(comparator));
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDTO project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @NotNull final ProjectDTO project = findOneById(userId, id);
        project.setStatus(status);
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public ProjectDTO add(@Nullable final String userId, @Nullable final ProjectDTO model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        model.setUserId(userId);
        repository.save(model);
        return model;
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findByUserId(userId);
    }

    @NotNull
    @Override
    public ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO model = repository.findOneByUserIdAndId(userId, id);
        if (model == null) throw new ModelNotFoundException();
        return model;
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteByUserId(userId);
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final String userId, @Nullable final ProjectDTO model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        if (!userId.equals(model.getUserId())) throw new ModelNotFoundException();
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        findOneById(userId, id);
        repository.deleteOneByUserIdAndId(userId, id);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new IdEmptyException();
        return repository.countByUserId(userId);
    }

    @NotNull
    private String getSortedColumn(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        else if (comparator == StatusComparator.INSTANCE) return "status";
        else return "name";
    }

}
