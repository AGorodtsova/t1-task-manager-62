package ru.t1.gorodtsova.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.gorodtsova.tm.service.DomainService;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public final class Backup {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    @Autowired
    private Bootstrap bootstrap;

    public void start() {
        //load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        bootstrap.getDomainService().saveDataBackup();
    }

    public void load() {
        if (!Files.exists(Paths.get(DomainService.FILE_BACKUP))) return;
        bootstrap.getDomainService().loadDataBackup();
    }

}
